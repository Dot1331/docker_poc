# syntax=docker/dockerfile:1

##
## Build
##
FROM golang:1.17-buster AS build

RUN useradd -u 10001 scratchuser

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN  CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /golang-ping

##
## Deploy
##
FROM scratch

COPY --from=build /etc/passwd /etc/passwd

WORKDIR /

COPY --from=build /golang-ping /golang-ping

USER scratchuser

EXPOSE 8080

ENTRYPOINT ["/golang-ping"]