Prerequisite 

•	Golang - https://golang.org/doc/install
•	Docker - https://www.docker.com/

Solution:

Steps:
•	First create a simple Golang program.
•	Create a multi-stage docker file.
•	Create a new user to use instead of root
•	Build the Golang image using docker.
•	Run the image and check for functionality of through the container.

Procedure:
Golang Program:
Creating a simple web server using /labstack/echo package, which provides a simple web service framework along with logger, middleware and recovery mechanism, making use of go mod for package management.

Docker File:
First, we will start by creating a docker file for the multi-stage build. We will use the Golang version 1.17-buster as the base image for building the Golang image, In the second stage we will use Scratch as the base image, where we will copy the built binary that is the first stage artifact to the Scratch base and execute the binary. We will make use of the port 8080 and also expose it in the docker file.
Creating a new user named scratchuser in the first stage and making use of same user’s credentials by copying the /etc/passwd to the new stage, now declaring that user in the final stage before the entrypoint, so by this we achieve security of container, where the container is running only with non root user peviliges.

